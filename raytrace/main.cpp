#include <string>
#include <math.h>
#include <CImg.h>
#include <fstream>
#include <istream>
#include <sstream>

//////////////////////////////////////////////////
//											   //
// data structures for rendering scene  	  //
//											 //
//////////////////////////////////////////////
// a basic 3d vector
struct vec3
{
	float x;
	float y;
	float z;
};

// material property information
struct Color
{
	vec3 ambient;
	vec3 specular;
	vec3 diffuse;
	float shi;
};

// color representation as 8bit per color chanel
struct ColorRGB
{
	unsigned int r;
	unsigned int g;
	unsigned int b;
};

// a sphere object representation
struct Sphere
{
	vec3 position;
	float radius;
	Color *color;
};

// a plane object representation
struct Plane
{
	vec3 normal;
	vec3 position;
	Color *color;
};

// a ray
struct Ray
{
	vec3 origin;
	vec3 direction; // unit vector
};

// a light object representation
struct Light
{
	vec3 pos;
	vec3 col;
};

// a camera object representation
struct Camera
{
	vec3 pos;
	float fov;
	float focalLength;
	float aspectRatio;
};

// util enum for parsing files
enum CurrentObject
{
	CAMERA, PLANE, SPHERE, LIGHT
};
//////////////////////////////////////////////////
//											   //
// function defenitions for rendering scene	  //
//											 //
//////////////////////////////////////////////

/**
* @brief function to parse a scene file into a set of struct arrays
*
* @param fileName the files name
* @param s a pointer to a sphere pointer
* @param sphereCount a pointer to an unsigned int to store the amount of spheres
* @param p a pointer to a plane pointer where all planes will be stored
* @param planeCount a pointer to an unsigned int to store the amount of planes
* @param l pointer to an light pointer where all the lights will be stored
* @param lightCount pointer to an unsigned int where the amount of lights will be stored
* @param c a pointer to a camera to store camera information abount the scene
*/
void parseFile(std::string fileName, Sphere **s, unsigned int *sphereCount, Plane **p, unsigned int *planeCount, Light **l, unsigned int *lightCount, Camera *c);

/**
* @brief function to cast a ray against all spheres planes and lights
*
* @param Ray the ray being cast into the scene
* @param spheres a pointer to an array of spheres
* @param sphereCount the number of spheres in the spheres array
* @param planes a pointer to an array of planes
* @param planeCount the number of planes in the plane array
* @param lights a pointer to an array of lights
* @param lightCount the number of lights in the lights array
*
* @returns the color of the ray in 24 bit RGB color value
*/
ColorRGB castRay(Ray ray, Sphere *spheres, unsigned int spereCount, Plane *planes, unsigned int planeCount, Light *lights, unsigned int lightCount);

/**
* @brief function to cast a shadow ray towards a light and verify if that shadow ray intersects an object
*
* @param Ray the ray being cast from a collision point to a light
* @param spheres a pointer to an array of spheres
* @param sphereCount the number of spheres in the spheres array
* @param planes a pointer to an array of planes
* @param planeCount the number of planes in the plane array
*
* @returns true if no object is intersected or false if an object intersects the ray
*/
bool castShadowRay(Ray ray, Sphere *spheres, unsigned int spereCount, Plane *planes, unsigned int planeCount);

/**
* @brief function to check if a ray intersects with a sphere
*
* @param ray the Ray being cast
* @param sphere the Sphere to check against
* @param inter the point of intersection between an object and a ray
*
* @returns true if the sphere is intersected and false otherwise
*/
bool checkIntersection(Ray ray, const Sphere &sphere, vec3 &inter);

/**
* @brief function to check if a Ray intersects with a Plane
*
* @param ray the Ray being cast
* @param plane the Plane to check against
* @param inter the point of intersection between an object and a ray
*
* @returns true if the plane is intersected and false otherwise
*/
bool checkIntersection(Ray ray, const Plane &sphere, vec3 &inter);

// vector math (these are pretty self explanitory)
inline vec3 operator*(const vec3 &l, const vec3 &r);

inline vec3 operator-(const vec3 &l, const vec3 &r);

inline vec3 operator+(const vec3 &l, const vec3 &r);

inline float magnitude(const vec3 &vec);

inline vec3 normalize(const vec3 &vec);

inline float degToRad(const float angle);

inline float dot(const vec3 &v1, const vec3 &v2);

vec3 reflect(vec3 d, vec3 n);

// ligth calculations
/**
* @brief function to calculate the diffuse color of a surface at a point
*
* @param vertex_normal the normal at the point
* @param vertex_light direction of th light to the point
* @param light_color color of the light
* @param diffuse_color the diffuse color of the surface
*
* @returns the diffuse color
*/
vec3 computeDiffuse(vec3 vertex_normal, vec3 vertex_ligth, vec3 light_color, vec3 diffuse_color);

/**
* @brief function to calculate the specular color of a surface at a point
*
* @param vertex_normal the normal at the point
* @param vertex_light direction of th light to the point
* @param the point at which the light is hitting
* @param light_color color of the light
* @param specular_color the diffuse color of the surface
* @param shininess the shininess coefficient ALPHA
*
* @returns the specular color
*/
vec3 computeSpecular(vec3 vertex_normal, vec3 vertex_ligth, vec3 cam_position, vec3 frag_position, vec3 light_color, vec3 specular_color, float shininess);

// file helpers
/**
* @brief function to convert a space delimited string of numbers into a vec3 eg "0 10 -2"
*
* @param vecToken the string to convert
*
* @returns the vec3 value of the string
*/
vec3 parseVecToken(std::string &vecToken);

/**
* @brief function to convert a string representation of a float into a float
*
* @param floatToken the string to convert
*
* @returns the float value equivalent of the string
*/
float parseFloatToken(std::string floatToken);

int main(int argc, char** argv)
{
	// define scene vars
	Sphere *spheres;
	unsigned int sphereCount;
	Plane *planes;
	unsigned int planeCount;
	Light *lights;
	unsigned int lightCount;
	Camera camera;

	std::string fileName = "./res/scenes/scene1"; // fallback if no argument is provided for scene file
	// get scene file name from command line if available
	if (argc > 1)
	{
		fileName = argv[1];
	}

	// generate input and output file names from the command line arg
	std::string inFile = fileName + ".txt";
	std::string outFile = fileName + "_out.bmp";

	// parse file into scene data structures
	parseFile(inFile, &spheres, &sphereCount, &planes, &planeCount, &lights, &lightCount, &camera);

	// compute width and height of image
	int HEIGHT = tan(degToRad(camera.fov) / 2) * (2 * camera.focalLength);
	int WIDTH = HEIGHT * camera.aspectRatio;

	// compute half offsets of image width and height for rendering
	int halfHeight = HEIGHT / 2;
	int halfWidth = WIDTH / 2;

	// generate buffer to store image color data
	ColorRGB *imgBuffer = new ColorRGB[WIDTH * HEIGHT];

	for (int y = 0; y < HEIGHT; ++y)
	{
		for (int x = 0; x < WIDTH; x++)
		{
			vec3 toPoint = { x - halfWidth, halfHeight - y, -camera.focalLength };
			toPoint = toPoint - camera.pos;
			Ray myRay = { camera.pos, normalize(toPoint) };
			imgBuffer[x + (WIDTH * y)] = castRay(myRay, spheres, sphereCount, planes, planeCount, lights, lightCount); // assign color to pixel in image buffer
		}
	}
	cimg_library::CImg<unsigned char> img(WIDTH, HEIGHT, 1, 3, 0);
	// write image buffer to CImg object
	for (int i = 0; i < WIDTH * HEIGHT; ++i)
	{
		int y = i / WIDTH;
		int x = i - (WIDTH * y);
		*(img.data(x, y, 0, 0)) = imgBuffer[i].r & 0xFF; // int should be from 0 to 255 so take only first byte
		*(img.data(x, y, 0, 1)) = imgBuffer[i].g & 0xFF; // int should be from 0 to 255 so take only first byte
		*(img.data(x, y, 0, 2)) = imgBuffer[i].b & 0xFF; // int should be from 0 to 255 so take only first byte
	}
	img.save(outFile.c_str()); // write resulting image to file
	cimg_library::CImgDisplay main_disp(img, "result"); // display image
	while (!main_disp.is_closed()) {
		main_disp.wait();
	}

	return 0;
}

void parseFile(std::string fileName, Sphere **s, unsigned int *sphereCount, Plane **p, unsigned int *planeCount, Light **l, unsigned int *lightCount, Camera *c)
{
	std::ifstream file(fileName);
	std::string line;
	*sphereCount = 0;
	*planeCount = 0;
	*lightCount = 0;
	// first pass to count data objects
	while (std::getline(file, line))
	{
		if (line == "sphere")
		{
			++(*sphereCount);
		} 
		else if (line == "plane")
		{
			++(*planeCount);
		}
		else if (line == "light")
		{
			++(*lightCount);
		}
	}
	// clear file for second pass
	file.clear();
	file.seekg(0, std::ios_base::beg);
	std::getline(file, line); // skip first line since it is a number that I don't use
	// initialize data structures
	*s = new Sphere[*sphereCount];
	*p = new Plane[*planeCount];
	*l = new Light[*lightCount];

	// initialize cursors to write to data structures
	unsigned int sCursor = 0;
	unsigned int pCursor = 0;
	unsigned int lCursor = 0;
	unsigned int *currentCursor = NULL;

	CurrentObject currentObj;

	while (std::getline(file, line))
	{
		// check if line is object type
		if (line == "sphere")
		{

			if (currentCursor) ++(*currentCursor);
			currentCursor = &sCursor;
			currentObj = SPHERE;
			(*s)[sCursor].color = new Color();
			continue;
		}
		else if (line == "plane")
		{
			if (currentCursor) ++(*currentCursor);
			currentCursor = &pCursor;
			currentObj = PLANE;
			(*p)[pCursor].color = new Color;
			continue;
		}
		else if (line == "light")
		{
			if (currentCursor) ++(*currentCursor);
			currentCursor = &lCursor;
			currentObj = LIGHT;
			continue;
		}
		else if (line == "camera")
		{
			currentObj = CAMERA;
			continue;
		}

		std::string token;
		std::string data;
		std::istringstream lineStream(line);
		std::getline(lineStream, token, ':');
		std::getline(lineStream, data, ' '); // flush initial space
		std::getline(lineStream, data);
		// parse data based on what object type is currently being read
		switch (currentObj)
		{
		case CAMERA:
			if (token == "pos")
			{
				c->pos = parseVecToken(data);
			}
			else if (token == "fov")
			{
				c->fov = parseFloatToken(data);
			}
			else if (token == "f")
			{
				c->focalLength = parseFloatToken(data);
			}
			else if (token == "a")
			{
				c->aspectRatio = parseFloatToken(data);
			}
			break;
		case SPHERE:
			if (token == "pos")
			{
				(*s)[sCursor].position = parseVecToken(data);
			}
			else if (token == "rad")
			{
				(*s)[sCursor].radius = parseFloatToken(data);
			}
			else if (token == "amb")
			{
				(*s)[sCursor].color->ambient = parseVecToken(data);
			}
			else if (token == "dif")
			{
				(*s)[sCursor].color->diffuse = parseVecToken(data);
			}
			else if (token == "spe")
			{
				(*s)[sCursor].color->specular = parseVecToken(data);
			}
			else if (token == "shi")
			{
				(*s)[sCursor].color->shi = parseFloatToken(data);
			}
			break;
		case PLANE:
			if (token == "pos")
			{
				(*p)[pCursor].position = parseVecToken(data);
			}
			else if (token == "nor")
			{
				(*p)[pCursor].normal = parseVecToken(data);
			}
			else if (token == "amb")
			{
				(*p)[pCursor].color->ambient = parseVecToken(data);
			}
			else if (token == "dif")
			{
				(*p)[pCursor].color->diffuse = parseVecToken(data);
			}
			else if (token == "spe")
			{
				(*p)[pCursor].color->specular = parseVecToken(data);
			}
			else if (token == "shi")
			{
				(*p)[pCursor].color->shi = parseFloatToken(data);
			}
			break;
		case LIGHT:
			if (token == "pos")
			{
				(*l)[lCursor].pos = parseVecToken(data);
			} 
			else if (token == "col")
			{
				(*l)[lCursor].col = parseVecToken(data);
			}
			break;
		}
	}
}

// returns color
ColorRGB castRay(Ray ray, Sphere *spheres, unsigned int sphereCount, Plane *planes, unsigned int planeCount, Light *lights, unsigned int lightCount)
{
	vec3 collision = { 0, 0, 0 };
	vec3 collisionNormal = { 0, 0, 0 };
	Color *collisionColor = NULL;
	vec3 collisionScalar = { 0.5f, 0.5f, 0.5f };
	bool hasCollided = false;

	for (unsigned int i = 0; i < sphereCount; ++i) 
	{
		 vec3 myCollision; // check if smallest
		 
		 if (hasCollided)
		 {
			 // check that there actually was a collision
			 if (checkIntersection(ray, spheres[i], myCollision) && magnitude(myCollision - ray.origin) < magnitude(collision - ray.origin))
			 {
				 vec3 toCol = myCollision - spheres[i].position;
				 collisionColor = spheres[i].color;
				 collisionNormal = normalize(toCol);
				 collision = myCollision + (collisionNormal * collisionScalar);
			 } 
		 }
		 else
		 {
			 // check that there actually was a collision
			 if (checkIntersection(ray, spheres[i], myCollision))
			 {
				 vec3 toCol = myCollision - spheres[i].position;
				 collisionColor = spheres[i].color;
				 collisionNormal = normalize(toCol);
				 collision = myCollision + (collisionNormal * collisionScalar);
				 hasCollided = true; // first collision has occured
			 }
		 }
	}

	for (unsigned int i = 0; i < planeCount; ++i)
	{
		vec3 myCollision; // check if smallest
		if (hasCollided)
		{
			// check that there actually was a collision
			if (checkIntersection(ray, planes[i], myCollision) && magnitude(myCollision - ray.origin) < magnitude(collision - ray.origin))
			{
				collision = myCollision;
				collisionColor = planes[i].color;
				collisionNormal = planes[i].normal;
			}
		}
		else
		{
			// check that there actually was a collision
			if (checkIntersection(ray, planes[i], myCollision))
			{
				collision = myCollision;
				collisionColor = planes[i].color;
				collisionNormal = planes[i].normal;
				hasCollided = true; // first collision has occured
			}
		}
	}

	// check if collision with geometry occured before trying to cast shadow ray
	if (hasCollided) {
		vec3 fragColor = collisionColor->ambient;
		for (unsigned int i = 0; i < lightCount; ++i)
		{
			vec3 toLight = lights[i].pos - collision;
			Ray shadowRay = { collision, normalize(toLight) };
			if (castShadowRay(shadowRay, spheres, sphereCount, planes, planeCount)) {
				vec3 diffuse = computeDiffuse(collisionNormal, shadowRay.direction, lights[i].col, collisionColor->diffuse);
				vec3 spec = computeSpecular(collisionNormal, shadowRay.direction, ray.origin, collision, lights[i].col, collisionColor->specular, collisionColor->shi);
				vec3 fullCol = (diffuse + spec);
				// compute phong and alter base color
				fragColor = fragColor + fullCol;
				// fragColor = diffuse;
			}
		}
		fragColor = { fminf(fragColor.x, 1.0f), fminf(fragColor.y, 1.0f), fminf(fragColor.z, 1.0f) }; // clamp color to 1 since values greater than 1 would mess up integer color value
		return { (unsigned int)(255.0f * fragColor.x), (unsigned int)(255.0f * fragColor.y), (unsigned int)(255.0f * fragColor.z) }; // set color to integer value
	}
	// return black if no collision occured
	return { 0, 0, 0 };
}

bool castShadowRay(Ray ray, Sphere *spheres, unsigned int sphereCount, Plane *planes, unsigned int planeCount)
{
	for (unsigned int i = 0; i < sphereCount; ++i)
	{
		vec3 myColision;
		if (checkIntersection(ray, spheres[i], myColision))
		{
			return false; // since no reflection is being calculated short circuit shadow ray to save on processing time
		}
	}

	for (unsigned int i = 0; i < planeCount; ++i)
	{
		vec3 myColision;
		if (checkIntersection(ray, spheres[i], myColision))
		{
			return false; // since no reflection is being calculated short circuit shadow ray to save on processing time
		}
	}

	return true; // path to light is clear
}


bool checkIntersection(Ray ray, const Sphere &sphere, vec3 &inter)
{
	float a, b, c;
	a = 1;

	b = 2 * (
		ray.direction.x * (ray.origin.x - sphere.position.x)
		+ ray.direction.y * (ray.origin.y - sphere.position.y)
		+ ray.direction.z * (ray.origin.z - sphere.position.z));

	c = pow(ray.origin.x - sphere.position.x, 2)
		+ pow(ray.origin.y - sphere.position.y, 2)
		+ pow(ray.origin.z - sphere.position.z, 2)
		- pow(sphere.radius, 2);

	float square = pow(b, 2) - 4 * c;
	if (square > 0) {
		float res1 = (-b + sqrt(square)) / 2.0f;
		float res2 = (-b - sqrt(square)) / 2.0f;
		// make sure that negative values are excluded
		if (res1 > 0.0f && res2 > 0.0f) {
			float hit = fminf(res1, res2);
			vec3 dir = { ray.direction.x * hit, ray.direction.y * hit, ray.direction.z * hit };
			inter = ray.origin + dir; // compute actual intersection point and store it in inter
			return true;
		}
	}

	return false; // no collision
}

bool checkIntersection(Ray ray, const Plane &plane, vec3 &inter)
{
	float denom = dot(ray.direction, plane.normal);
	// ensure greter than 0 to avoid division by 0 and include a slight bias to reduce float inaccuracy
	if (abs(denom) > 0.0001f)
	{
		float t = dot(plane.position - ray.origin, plane.normal) / denom;
		if (t > 0.001) 
		{
			inter = { ray.origin.x + ray.direction.x * t, ray.origin.y + ray.direction.y * t, ray.origin.z + ray.direction.z * t };
			return true;
		}
	}

	return false;
}

// YAY MATH
vec3 operator*(const vec3 &l, const vec3 &r)
{
	return { l.x * r.x, l.y * r.y, l.z * r.z };
}

vec3 operator-(const vec3 &l, const vec3 &r)
{
	return  { l.x - r.x, l.y - r.y, l.z - r.z };
}

vec3 operator+(const vec3 &l, const vec3 &r)
{
	return  { l.x + r.x, l.y + r.y, l.z + r.z };
}

float magnitude(const vec3 &vec) {
	return sqrt(pow(vec.x, 2) + pow(vec.y, 2) + pow(vec.z, 2));
}

vec3 normalize(const vec3 &vec) {
	float mag = magnitude(vec);
	return { vec.x / mag, vec.y / mag, vec.z / mag };
}

float degToRad(const float angle)
{
	float pi = 3.1415926f;
	return (pi * angle) / 180;
}

float dot(const vec3 &v1, const vec3 &v2)
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

// formula based on http://www.3dkingdoms.com/weekly/weekly.php?a=2
vec3 reflect(vec3 d, vec3 n)
{
	float dotProdTwo = dot(d, n) * 2.0f;
	vec3 res = { dotProdTwo * n.x, dotProdTwo * n.y, dotProdTwo * n.z };
	return res - d;
}

vec3 computeSpecular(vec3 vertex_normal, vec3 vertex_ligth, vec3 cam_position, vec3 frag_position, vec3 light_color, vec3 specular_color, float shininess) 
{
	vec3 viewDir = normalize(cam_position - frag_position);
	vec3 zero = { 0, 0, 0 };
	vec3 revLigt = zero - vertex_ligth;
	vec3 reflectDir = reflect(vertex_ligth, vertex_normal);
	reflectDir = normalize(reflectDir);
	float spec = pow(fmaxf(dot(viewDir, reflectDir), 0.0), shininess);
	vec3 specCol = { specular_color.x * spec, specular_color.y * spec, specular_color.z * spec };
	return  specCol * light_color;
}


vec3 computeDiffuse(vec3 vertex_normal, vec3 vertex_ligth, vec3 light_color, vec3 diffuse_color) {
	float brigthness = fmaxf(dot(vertex_normal, vertex_ligth), 0.0f);
	vec3 light = { light_color.x * brigthness, light_color.y * brigthness, light_color.z * brigthness };
	return light * diffuse_color;
}

vec3 parseVecToken(std::string &vecString)
{
	vec3 result;
	std::string x, y, z;
	std::istringstream ss(vecString);
	std::getline(ss, x, ' ');
	std::getline(ss, y, ' ');
	std::getline(ss, z);
	
	return { std::stof(x), std::stof(y), std::stof(z) };
}

float parseFloatToken(std::string floatToken)
{
	return std::stof(floatToken);
}
